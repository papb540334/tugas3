package com.example.hellomrhead;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private ImageView hairImageView, eyebrowImageView, eyesImageView, beardImageView, moustacheImageView, bodyImageView;
    private CheckBox rambutCheckBox, alisCheckBox, kumisCheckBox, jenggotCheckBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Inisialisasi ImageView dan CheckBox
        hairImageView = findViewById(R.id.hair);
        eyebrowImageView = findViewById(R.id.eyebrow);
        eyesImageView = findViewById(R.id.eyes);
        beardImageView = findViewById(R.id.beard);
        moustacheImageView = findViewById(R.id.moustache);
        bodyImageView = findViewById(R.id.body);

        rambutCheckBox = findViewById(R.id.pilihRambut);
        alisCheckBox = findViewById(R.id.pilihAlis);
        kumisCheckBox = findViewById(R.id.pilihKumis);
        jenggotCheckBox = findViewById(R.id.pilihJenggot);

        rambutCheckBox.setChecked(true);
        alisCheckBox.setChecked(true);
        kumisCheckBox.setChecked(true);
        jenggotCheckBox.setChecked(true);


        // Atur OnClickListener untuk setiap CheckBox
        rambutCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rambutCheckBox.isChecked()) {
                    hairImageView.setVisibility(View.VISIBLE);
                } else {
                    hairImageView.setVisibility(View.INVISIBLE);
                }
            }
        });

        alisCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alisCheckBox.isChecked()) {
                    eyebrowImageView.setVisibility(View.VISIBLE);
                } else {
                    eyebrowImageView.setVisibility(View.INVISIBLE);
                }
            }
        });

        kumisCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (kumisCheckBox.isChecked()) {
                    moustacheImageView.setVisibility(View.VISIBLE);
                } else {
                    moustacheImageView.setVisibility(View.INVISIBLE);
                }
            }
        });

        jenggotCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (jenggotCheckBox.isChecked()) {
                    beardImageView.setVisibility(View.VISIBLE);
                } else {
                    beardImageView.setVisibility(View.INVISIBLE);
                }
            }
        });

         //Atur OnClickListener untuk TextView
        TextView textView = findViewById(R.id.textView);


        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Hello, Mr. Head!", Toast.LENGTH_LONG).show();
            }
        });

    }
}